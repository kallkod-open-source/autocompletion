#include <gtest/gtest.h>
#include "command_hierarchy.h"
#include <vector>
#include <list>

class Command_hamming_distance_hiearchyTest : public::testing::Test
{
public:
    kallkod::Completion c;

    virtual void SetUp()
    {
        c.add_command("test");
        c.add_command("tost", { "yes" , "no", "noo", "noon" });
        c.add_command("text");
    }
};

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_0)
{
    const char* const name[] = { "program_name", "tttt" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 0);
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_1)
{
    const char* const name[] = { "program_name", "tost" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 4);
    ASSERT_EQ(out.at(0), "yes");
    ASSERT_EQ(out.at(1), "no");
    ASSERT_EQ(out.at(2), "noo");
    ASSERT_EQ(out.at(3), "noon");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_2)
{
    const char* const name[] = { "program_name", "test" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 0);
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_3)
{
    const char* const name[] = { "program_name", "tes" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 1);
    ASSERT_EQ(out.at(0), "test");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_4)
{
    const char* const name[] = { "program_name", "te" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 2);
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_missprint_1)
{
    const char* const name[] = { "program_name", "tess" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 1);
    ASSERT_EQ(out.at(0), "test");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_missprint_2)
{
    const char* const name[] = { "program_name", "rest" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 1);
    ASSERT_EQ(out.at(0), "test");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_missprint_3)
{
    const char* const name[] = { "program_name", "tett" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 2);
    ASSERT_EQ(out.at(0), "test");
    ASSERT_EQ(out.at(1), "text");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_one_symbol)
{
    const char* const name[] = { "program_name", "t" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 3);
    ASSERT_EQ(out.at(0), "test");
    ASSERT_EQ(out.at(1), "tost");
    ASSERT_EQ(out.at(2), "text");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_option_1)
{
    const char* const name[] = { "program_name", "to" , "ye" };

    const int argc = 3;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 2);
    ASSERT_EQ(out.at(0), "tost");
    ASSERT_EQ(out.at(1), "yes");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_option_2)
{
    const char* const name[] = { "program_name", "to", "n" };

    const int argc = 3;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 3);
    ASSERT_EQ(out.at(0), "no");
    ASSERT_EQ(out.at(1), "noo");
    ASSERT_EQ(out.at(2), "noon");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_option_3)
{
    const char* const name[] = { "program_name", "tost", "n" };

    const int argc = 3;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 3);
    ASSERT_EQ(out.at(0), "no");
    ASSERT_EQ(out.at(1), "noo");
    ASSERT_EQ(out.at(2), "noon");
}

TEST_F(Command_hamming_distance_hiearchyTest, Find_hamming_distance_command_option_4)
{
    const char* const name[] = { "program_name", "to", "bee" };

    const int argc = 3;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 0);
}

class Command_longTest : public::testing::Test
{
public:
    kallkod::Completion c;

    virtual void SetUp()
    {
        c.add_command("MARTHA");
        c.add_command("MURUTHA");
        c.add_command("MUTAMA");
        c.add_command("MURKA");
    }
};

TEST_F(Command_longTest, Find_hamming_distance_one_symbol)
{
    const char* const name[] = { "program_name", "M" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 4);
    ASSERT_EQ(out.at(0), "MARTHA");
    ASSERT_EQ(out.at(1), "MURUTHA");
    ASSERT_EQ(out.at(2), "MUTAMA");
    ASSERT_EQ(out.at(3), "MURKA");
}

TEST_F(Command_longTest, Find_hamming_distance_two_symbols)
{
    const char* const name[] = { "program_name", "MU" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 3);
    ASSERT_EQ(out.at(0), "MURUTHA");
    ASSERT_EQ(out.at(1), "MUTAMA");
    ASSERT_EQ(out.at(2), "MURKA");
}

TEST_F(Command_longTest, Find_jaro_winkler_distance_command_1)
{
    const char* const name[] = { "program_name", "MARTHA" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 0);
}

TEST_F(Command_longTest, Find_jaro_winkler_distance_command_2)
{
    const char* const name[] = { "program_name", "MARHTA" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 1);
    ASSERT_EQ(out.at(0), "MARTHA");
}

TEST_F(Command_longTest, Find_jaro_winkler_distance_command_3)
{
    const char* const name[] = { "program_name", "XXXXXX" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 0);
}

class Command_shortTest : public::testing::Test
{
public:
    kallkod::Completion c;

    virtual void SetUp()
    {
        c.add_command("MAR");
        c.add_command("MUR");
        c.add_command("MAT");
    }
};

TEST_F(Command_shortTest, Find_hamming_distance_command_1)
{
    const char* const name[] = { "program_name", "MAR" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 0);
}

TEST_F(Command_shortTest, Find_hamming_distance_command_2)
{
    const char* const name[] = { "program_name", "MARTHA" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 1);
    ASSERT_EQ(out.at(0), "MAR");
}

TEST_F(Command_shortTest, Find_hamming_distance_command_3)
{
    const char* const name[] = { "program_name", "MUTUMA" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 2);
    ASSERT_EQ(out.at(0), "MUR");
    ASSERT_EQ(out.at(1), "MAT");
}

TEST_F(Command_shortTest, Find_hamming_distance_command_4)
{
    const char* const name[] = { "program_name", "MAKUNA" };

    const int argc = 2;
    std::vector<std::string> out;
    out = c.command_hierarchy(argc, name);

    ASSERT_EQ(out.size(), 2);
    ASSERT_EQ(out.at(0), "MAR");
    ASSERT_EQ(out.at(1), "MAT");
}
