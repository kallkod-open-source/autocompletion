#include <gtest/gtest.h>
#include "command_hierarchy.h"
#include <string>
#include <cmath>

using namespace kallkod;

TEST(hamming_distance, Test_0)
{
    const char* name = "hello";
    const std::string command = "hello";

    std::size_t dist = hamming_distance(name, command);

    ASSERT_EQ(dist, 0);
}

TEST(hamming_distance, Test_1)
{
    const char* name = "hollo";
    const std::string command = "hello";

    std::size_t dist = hamming_distance(name, command);

    ASSERT_EQ(dist, 1);
}

TEST(hamming_distance, Test_2)
{
    const char* name = "oollo";
    const std::string command = "hello";

    std::size_t dist = hamming_distance(name, command);

    ASSERT_EQ(dist, 2);
}

TEST(hamming_distance, Test_max)
{
    const char* name = "prive";
    const std::string command = "hello";

    std::size_t dist = hamming_distance(name, command);

    ASSERT_EQ(dist, 5);
}

TEST(hamming_distance, Test_number1)
{
    const char* name = "1234";
    const std::string command = "123";

    std::size_t dist = hamming_distance(name, command);

    ASSERT_EQ(dist, 0);
}

TEST(hamming_distance, Test_number2)
{
    const char* name = "123";
    const std::string command = "1234";

    std::size_t dist = hamming_distance(name, command);

    ASSERT_EQ(dist, 0);
}

TEST(prefix_distance, AARTHM_MARTHA)
{
    const char* name = "AARTHM";
    const std::string command = "MARTHA";

    std::size_t dist = prefix_distance(name, command);

    ASSERT_EQ(dist, 0);
}

TEST(prefix_distance, MARTHA_MARHTA)
{
    const char* name = "MARTHA";
    const std::string command = "MARHTA";

    std::size_t dist = prefix_distance(name, command);

    ASSERT_EQ(dist, 3);
}

TEST(prefix_distance, MARTHA_MARTHA)
{
    const char* name = "MARTHA";
    const std::string command = "MARTHA";

    std::size_t dist = prefix_distance(name, command);

    ASSERT_EQ(dist, 6);
}

TEST(Jaro_distance, MARTHA_MARTHA)
{
    const char* name = "MARTHA";
    const std::string command = "MARTHA";

    float dist = jaro_winkler_distance(name, command, 1);

    EXPECT_FLOAT_EQ(dist, 1.0);
}

TEST(Jaro_distance, MARTHA_M)
{
    const char* name = "MARTHA";
    const std::string command = "M";

    float dist = jaro_winkler_distance(name, command, 0.8);

    EXPECT_FLOAT_EQ(dist, 0.72222222);
}

TEST(Jaro_distance, MARTHA_MARHTA)
{
    const char* name = "MARHTA";
    const std::string command = "MARTHA";

    float dist = jaro_winkler_distance(name, command, 0.95);

    EXPECT_FLOAT_EQ(dist, 0.9444444);
}

TEST(jaro_winkler_distance, MARTHA_MARHTA)
{
    const char* name = "MARHTA";
    const std::string command = "MARTHA";

    float dist = jaro_winkler_distance(name, command, 0.7);

    EXPECT_FLOAT_EQ(dist, 0.96111111);
}

TEST(Jaro_distance, DIXON_DICKSONX)
{
    const char* name = "DIXON";
    const std::string command = "DICKSONX";

    float dist = jaro_winkler_distance(name, command, 0.8);

    EXPECT_FLOAT_EQ(dist, 0.7666666);
}

TEST(jaro_winkler_distance, DIXON_DICKSONX)
{
    const char* name = "DIXON";
    const std::string command = "DICKSONX";

    float dist = jaro_winkler_distance(name, command, 0.7);

    EXPECT_FLOAT_EQ(dist, 0.81333333);
}

TEST(Jaro_distance, DWAYNE_DUANE)
{
    const char* name = "DWAYNE";
    const std::string command = "DUANE";

    float dist = jaro_winkler_distance(name, command, 0.85);

    EXPECT_FLOAT_EQ(dist, 0.82222222);
}

TEST(jaro_winkler_distance, DWAYNE_DUANE)
{
    const char* name = "DWAYNE";
    const std::string command = "DUANE";

    float dist = jaro_winkler_distance(name, command, 0.7);

    EXPECT_FLOAT_EQ(dist, 0.84);
}

TEST(jaro_winkler_distance, HHHHH_DUANE)
{
    const char* name = "HHHHH";
    const std::string command = "DUANE";

    float dist = jaro_winkler_distance(name, command, 0.7);

    EXPECT_TRUE(std::isnan(dist));
}
