#include "command_hierarchy.h"
#include <iostream>
#include <list>
#include <map>
#include <iterator>
#include <cstring>
#include <cmath>

namespace {
const std::string ROOT_NAME="Base command level";
}

namespace kallkod {
void Completion::add_command(const std::string& command_name, const std::initializer_list<std::string>& option_list)
{
    h.insert({ROOT_NAME,command_name});

    for(auto& option : option_list)
        h.insert({command_name, option});
}

void Completion::add_command(const std::string& command_name)
{
    h.insert({ROOT_NAME,command_name});
}

std::vector<std::string> Completion::command_hierarchy(const int argc, const char* const argv[])
{
    std::vector<std::string> result;

    if (argc == 1) return get_level(ROOT_NAME);

    auto command_it{find(argv[1], ROOT_NAME)};

    if (command_it.size() == 1 && argc == 2 && command_it.front()->second == argv[1])
    {
        auto command = command_it.front()->second;
        return get_level(command);
    }

    if (command_it.size() > 1)
    {
        return get_suggest(command_it);
    }

    if (command_it.size() == 1)
    {
        auto command = command_it.front()->second;

        if (argc <= 2)
        {
            result.emplace_back(command);
            return result;
        }

        auto options_it{find(argv[2], command)};
        if (options_it.size() == 1)
        {
            auto option = options_it.front()->second;
            result.emplace_back(command);
            result.emplace_back(option);
            return result;
        }

        if (options_it.size() > 1)
        {
            return get_suggest(options_it);
        }
    }

    return result;
}

std::ostream& operator<<(std::ostream& os, const Completion& c)
{
    auto commands = c.h.equal_range(ROOT_NAME);

    if (commands.first == commands.second)
    {
        os << "Commands not found";
        return os;
    }

    os << "Commands on " << commands.first->first << ":\n";
    for (auto it = commands.first; it != commands.second; ++it)
    {
         auto commands = it->second;
         os << commands << ", with options:\n";
         auto options = c.h.equal_range(commands);
         for (auto it = options.first; it != options.second; ++it)
         {
              auto option = it->second;
              os << "     " << option << "\n";
         }
    }

    return os;
}

std::list<Completion::Hierarchy::const_iterator> Completion::find(const char* name, const std::string& level)
{
    std::list<Completion::Hierarchy::const_iterator> result;
    auto commands = h.equal_range(level);

    std::string_view nam = name;

    size_t misprint_count_min = ~0U;

    for (auto it = commands.first; it != commands.second; ++it)
    {
        auto command = it->second;

        const std::size_t name_length = nam.length();
        const std::size_t command_length = command.length();

        if (name_length < hamming_limit || command_length < hamming_limit)
        {
            const auto misprint_count = hamming_distance(nam, command);
            if (misprint_count <= misprint_limit)
            {
                if (misprint_count < misprint_count_min)
                {
                    misprint_count_min = misprint_count;
                    result.clear();
                }
                if (misprint_count == misprint_count_min)
                {
                    result.emplace_back(it);
                }
            }
        }
        else
        {
            float winkler_min = 0;

            for (auto it = commands.first; it != commands.second; ++it)
            {
                auto command = it->second;
                const auto distance = jaro_winkler_distance(name, command, winkler_limit);

                if (!std::isnan(distance) && distance > jaro_winkler_search_limit)
                {
                    if (distance > winkler_min)
                    {
                        winkler_min = distance;
                        result.clear();
                    }
                    if (distance == winkler_min)
                    {
                        result.emplace_back(it);
                    }
                }
            }
        }
    }
    return result;
}

std::vector<std::string> Completion::get_level(const std::string& parent) const
{
    std::vector<std::string> result;
    auto commands = h.equal_range(parent);

    for (auto it = commands.first; it != commands.second; ++it)
    {
        result.emplace_back(it->second);
    }
    return result;
}

std::vector<std::string> Completion::get_suggest(
        const std::list<Completion::Hierarchy::const_iterator>& parent_it) const
{
    std::vector<std::string> result;
    for (auto it: parent_it)
    {
        result.emplace_back(it->second);
    }
    return result;
}

std::size_t hamming_distance(std::string_view name, const std::string& command)
{
    // We would like to keep it for the future expemiment with the different size strings
    //std::size_t dist_counter = abs(strlen(name) - command.length());
    std::size_t dist_counter = 0;
    auto command_it = command.begin();
    auto name_it = name.begin();
    for (;name_it != name.end() && command_it != command.end(); name_it ++, command_it++)
    {
        if (*name_it != *command_it) dist_counter += 1;
    }
    return dist_counter;
}

std::size_t prefix_distance(std::string_view name, const std::string& command)
{
    std::size_t result = 0;
    auto command_it = command.begin();
    auto name_it = name.begin();
    while (*name_it == *command_it && command_it != command.end())
    {
        name_it++;
        command_it++;
        result += 1;
    }
    return result;
}

float jaro_winkler_distance(std::string_view name, const std::string& command, const float winkler_limit)
{
    const std::size_t name_lenght = name.length();
    const std::size_t command_length = command.length();

    std::size_t coincidence_radius = std::max(name_lenght, command_length)/2 - 1;
    std::size_t t = 0, typo_pos = 0, typo_neg = 0;
    std::size_t m = 0;
    int command_index = 0;

    for (auto command_it = command.begin(); command_it != command.end(); command_it++)
    {
        command_index += 1;
        int name_index = 0;
        for (auto name_it = name.begin(); name_it != name.end(); name_it++)
        {
            name_index += 1;
            std::size_t dist = abs(command_index-name_index);
            if (*name_it == *command_it && dist < coincidence_radius)
            {
                m += 1;
                if (command_index > name_index) typo_pos += 1;
                if (command_index < name_index) typo_neg += 1;
            }
        }
    }

    if (typo_pos == typo_neg && typo_pos != 0) t = typo_pos;
    float d_Jaro = (float(m)/float(name_lenght) + float(m)/float(command_length) + float(m-t)/float(m))/3.0f;

    if (d_Jaro <= winkler_limit)
        return d_Jaro;

    std::size_t l = prefix_distance(name,command);
    float p = 0.1f;
    float d_Winkler = d_Jaro + l*p*(1-d_Jaro);

    return d_Winkler;
}

} // namespace kallkod
