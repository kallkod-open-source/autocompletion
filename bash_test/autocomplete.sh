# add to your .bashrc
# source /path/to/your/autocomplete.sh

_script()
{    
    COMPREPLY=()
    cur="${COMP_WORDS[${COMP_CWORD}]}"
    
    commands=$(./hierarchy ${COMP_WORDS[@]:1}) 
    
    COMPREPLY=( $(compgen -W "${commands}" -- ${cur}) ) 
    return 0
}
complete -o nospace -F _script test.sh
