#pragma once

#include <iostream>
#include <map>
#include <string>
#include <list>
#include <vector>

namespace kallkod {
class Completion
{
public:
    void add_command(const std::string& command_name, const std::initializer_list<std::string>& list);
    void add_command(const std::string& command_name);
    std::vector<std::string> command_hierarchy(const int argc, const char* const argv[]);

    /**
     * @brief Print command hierarhy to the given stream.
     * @param os Output stream.
     * @param c Completion object.
     * @return Stream given as a first parameter.
     */
    friend std::ostream& operator<<(std::ostream& os, const Completion& c);
private:
    // misprint_limit sets number of possible misprint in hamming algorithm
    size_t misprint_limit = 1;
    // number of symbols which are used in hamming algorithm
    size_t hamming_limit = 5;
    const float winkler_limit = 0.7;
    const float jaro_winkler_search_limit = 0.5;

    typedef std::multimap<std::string, std::string> Hierarchy;
    Hierarchy h;
    std::list<Hierarchy::const_iterator> find(const char* name, const std::string& level);

    /**
     * @brief Return children of a given level. If you give command name as a parameter,
     * then it will return all options available for that particular command.
     * @param string contains certain level command in the hierarchy
     * @return Vector of the strings.
     */
    std::vector<std::string> get_level(const std::string& parent) const;

    /**
     * @brief Return vector of suggestions for command/option
     * @param parent_it list of iterators belonging to Hierarchy
     * @return Vector of the strings.
     */
    std::vector<std::string> get_suggest(const std::list<Hierarchy::const_iterator>& parent_it) const;
};

/**
 * @brief Compares two strings and return numbers the discrepancy for the strings. Compares one by one strings
 * elements with the same index. Namber of comparison depends on the shortest sthing.
 * @param string contains certain level command in the hierarchy
 * @return interger number
 */
std::size_t hamming_distance(std::string_view name, const std::string& command);

/**
 * @return number of the same character form the beginnig
 */
std::size_t prefix_distance(std::string_view name, const std::string& command);

/**
* @brief jaro_winkler_distance return the string metric measuring distance between two sequences.
* Both string should be longer than 2 characters. See also https://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance
* @param winkler_limit is gain threshold.
* @return float number which represents Jaro Winkler distance.
* If distance is below winkler_limit, then Jaro distance is returned, otherwise it returns Jaro-Winkler distance.
*/
float jaro_winkler_distance(std::string_view name, const std::string& command, const float winkler_limit);
}
