#include "command_hierarchy.h"
#include <map>
#include <string>
#include <utility>
#include <vector>
#include <iostream>

int main(int argc, char *argv[])
{
    kallkod::Completion c;
    c.add_command("light");
    c.add_command("door", {"open", "close"});
    c.add_command("doll", {"in", "out", "lost", "last", "lasi"});
    c.add_command("desk", {"up", "down"});
    c.add_command("window", {"open", "close"});

    auto out = c.command_hierarchy(argc, argv);

    if (!out.empty())
    {
        auto it = out.cbegin();
        std::cout << *it;
        for(++it; it != out.cend(); ++it)
        {
            std::cout << " " << *it;
        }
        std::cout << std::endl;
    }

    return 0;
}
